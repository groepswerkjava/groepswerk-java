package be.thomasmore;

import java.io.File;
import java.util.Collection;
import java.util.List;

/**
 * Created by freek on 25-Sep-16. Edited by Nick on 30-Sep-16
 */
public interface ExcelReader {

    Collection<Student> readStudents(File inputXslxFile);

}
