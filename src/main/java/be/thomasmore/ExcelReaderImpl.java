package be.thomasmore;

import org.apache.poi.hssf.eventusermodel.HSSFEventFactory;
import org.apache.poi.hssf.eventusermodel.HSSFListener;
import org.apache.poi.hssf.eventusermodel.HSSFRequest;
import org.apache.poi.hssf.record.*;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by freek on 01-Oct-16.
 */
public class ExcelReaderImpl implements ExcelReader {


    public Collection<Student> readStudents(File inputXslxFile) {
        ArrayList<Student> students = new ArrayList<Student>();

        try {
            OPCPackage pkg = OPCPackage.open(inputXslxFile);
            XSSFReader r = new XSSFReader(pkg);
            SharedStringsTable sst = r.getSharedStringsTable();

            XMLReader parser = fetchSheetParser(sst, students);

            Iterator<InputStream> sheets = r.getSheetsData();
            while (sheets.hasNext()) {
                InputStream sheet = sheets.next();
                InputSource sheetSource = new InputSource(sheet);
                parser.parse(sheetSource);
                sheet.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return students;
    }

    private XMLReader fetchSheetParser(SharedStringsTable sst, Collection<Student> students) throws SAXException {
        XMLReader parser =
                XMLReaderFactory.createXMLReader();
        ContentHandler handler = new SheetHandler(sst, students);
        parser.setContentHandler(handler);
        return parser;
    }

    private static class SheetHandler extends DefaultHandler {
        private SharedStringsTable sst;
        private String lastContents;
        private boolean nextIsString;

        private int index = 0;
        private String[] values = new String[3];

        private Collection<Student> students;

        private SheetHandler(SharedStringsTable sst, Collection<Student> students) {
            this.sst = sst;
            this.students = students;
        }

        public void startElement(String uri, String localName, String name, Attributes attributes)  {
            if (name.equals("c")) {
                String cellType = attributes.getValue("t");
                if (cellType != null && cellType.equals("s")) {
                    nextIsString = true;
                } else {
                    nextIsString = false;
                }
            }
            lastContents = "";
        }

        public void endElement(String uri, String localName, String name) {
            if (nextIsString) {
                int idx = Integer.parseInt(lastContents);
                lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
                nextIsString = false;
            }

            if (name.equals("v")) {
                values[index] = lastContents;
                index++;
            }

            if (name.equals("row")) {
                index = 0;
                if(values[2].contains("@")) {

                    Student student = new Student();
                    student.setLastName(values[0]);
                    student.setFirstName(values[1]);
                    student.setEmail(values[2]);

                    students.add(student);
                }
            }
        }

        public void characters(char[] ch, int start, int length)
                throws SAXException {
            lastContents += new String(ch, start, length);
        }
    }

}

