package be.thomasmore;

import org.apache.poi.sl.usermodel.Resources;
import org.apache.poi.util.OOXMLLite;
import org.junit.Assert;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Unit test for simple App.
 */
public class ExcelReaderTest
{

    @org.junit.Test
    public void excelReaderTest(){
        ExcelReaderImpl eri = new ExcelReaderImpl();
        File xlsx = new File(getClass().getResource("/students.xlsx").getFile());

        ArrayList<Student> students = new ArrayList<Student>(eri.readStudents(xlsx));

        org.junit.Assert.assertEquals("Clinton", students.get(0).lastName);
        org.junit.Assert.assertEquals("Hillary", students.get(0).firstName);
        org.junit.Assert.assertEquals("hillary.clinton@gmail.com", students.get(0).email);

        org.junit.Assert.assertEquals("Trump", students.get(1).lastName);
        org.junit.Assert.assertEquals("Donald", students.get(1).firstName);
        org.junit.Assert.assertEquals("donald_j_trump@hotmail.com", students.get(1).email);
    }

}
